## Exercícios de Git
Este repositório trata-se de uma prática inicial sobre git. Visto em uma video-aula no YouTube que pode ser acessada por [aqui](https://www.youtube.com/watch?v=kB5e-gTAl_s&t=547s).

Os conceitos básicos vistos foram: versionamento, branches, pull request e gitignore.

Os comandos vistos foram

- `git init` - Inicializa um novo repositório
- `git add .` - Adiciona arquivos na área de staging: são os que vão fazer parte do próximo commit
- `git status` - Verifica statis atual do repositório local do git
- `git commit` - Cria um novo _commit_ com uma mensagem
- `git push` - Envia informações para a nuvem na branch atual
- `git branch` - Permite listar branches e ver qual é a ativa
- `git checkout nome-branch` - Altera para a branch nome-branch
- `git checkout -b "branch-origem" "branch-nova` - Cria uma brach branch-nova a partir de branch-origem
- `git merge staging` - Faz merge da branch staging na branch atual
- `git pull` - atualiza a branch atual
